# Frontend

Frontend to use with the backend in server mode

## Installation

After cloning the project run `yarn install`  
  
Then `yarn start` for development mode  
  
Or `yarn build` followed by `yarn prod` for production mode