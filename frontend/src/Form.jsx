import React, { useCallback, useEffect, useRef, useState } from "react";
import { loadFile } from "./Parse";
import { useResultsContext } from "./ResultsContext";
import useAxios from "axios-hooks";

export const FileForm = () => {
    const { setState } = useResultsContext();
    const [fileError, setFileError] = useState();
    const [{ data, loading, error }, execute] = useAxios({
        url: "http://localhost:8080/mow",
        method: "POST"
    }, { manual: true });

    useEffect(() => {
        setState(data);
    }, [data, setState]);

    const fileInputRef = useRef();
    const stepsCheckRef = useRef();
    const collisionCheckRef = useRef();

    const onSubmit = useCallback(async (evt) => {
        evt.preventDefault();
        setFileError();
        try {
            const requestBody = await loadFile(fileInputRef.current.files[0]);
            const params = {};
            if (stepsCheckRef.current.checked) {
                params.steps = true;
            }
            if (collisionCheckRef.current.checked) {
                params.collision = true;
            }
            execute({
                params: params,
                data: requestBody
            });
        } catch (err) {
            setFileError(err.message);
        }
    }, [execute]);

    return (
        <form className="card card-body" onSubmit={onSubmit}>
            <h4 className="card-title">Form</h4>
            <div className="mb-2">
                <label htmlFor="fileInput" className="form-label">File to parse</label>
                <input className="form-control" type="file" id="fileInput" ref={fileInputRef} />
            </div>
            <div className="mb-2">
                <div className="form-check form-check-inline">
                    <input className="form-check-input" type="checkbox" id="stepsCheckbox" value="steps" ref={stepsCheckRef} />
                    <label className="form-check-label" htmlFor="stepsCheckbox">Enable animation</label>
                </div>
                <div className="form-check form-check-inline">
                    <input className="form-check-input" type="checkbox" id="stepsCheckbox" value="collision" ref={collisionCheckRef} />
                    <label className="form-check-label" htmlFor="collisionCheckbox">Enable collision</label>
                </div>
            </div>
            <div className="text-end">
                <button type="submit" className="btn btn-primary" disabled={loading}>Submit</button>
            </div>
            <div className="alert alert-danger mt-3 mb-0" role="alert" hidden={!error && !fileError}>
                {error?.message || fileError}
            </div>
        </form>
    );
}