import React from "react";
import { useResultsContext } from "./ResultsContext";
import { Animation } from "./Animation";

export const Results = () => {
    const { state } = useResultsContext();

    if (state && "mowers" in state && Array.isArray(state.mowers)) {
        return (
            <div className="card card-body mt-3">
                <h4 className="card-title">Results</h4>
                <Animation state={state} />
                <div className="text-center">
                {state.mowers.map((mower, index) => <div key={index}>{mower.position}</div>)}
                </div>
            </div>
        ); 
    } else {
        return null;
    }
};