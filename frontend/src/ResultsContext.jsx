import React, { createContext, useContext, useMemo, useState } from "react";

export const ResultsContext = createContext();

export const useResultsContext = () => useContext(ResultsContext);

export const ResultsContextProvider = ({ children }) => {
    const [state, setState] = useState();

    const store = useMemo(() => ({ state, setState }), [state]);

    return (
        <ResultsContext.Provider value={store}>
            {children}
        </ResultsContext.Provider>
    );
};