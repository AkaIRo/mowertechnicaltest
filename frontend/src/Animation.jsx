import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useResultsContext } from "./ResultsContext";

const containsSteps = (state) => {
    return state?.mowers?.length > 0 && state.mowers[0].steps?.length > 0;
};

const Cell = ({ mapState, y, x }) => {
    const mowerOrientation = useMemo(() => {
        for (const mower of mapState.mowers) {
            if (mower.position.x === x && mower.position.y === y)
                return mower.position.orientation;
        }
        return false;
    }, [mapState, y, x]);

    return (
        <span style={{ width: "50px", height: "50px" }} className="d-flex align-items-center justify-content-center border fw-bold">
            {mowerOrientation}
        </span>
    );
};

const Row = ({ mapState, y }) => {
    const cells = useMemo(() => {
        const cells = [];
        for (let i = 0; i <= mapState.mapSize.x; i++) {
            cells.push(<Cell key={`${y}-${i}`} mapState={mapState} y={y} x={i} />);
        }
        return cells;
    }, [mapState, y]);

    return (
        <div className="d-flex justify-content-center">
            {cells}
        </div>
    );
};

const mapStateDefaultValue = {
    mapSize: { x: 0, y: 0 },
    currentCommand: "",
    mowers: [],
    currentMower: 0,
    currentStep: 0
};
const Map = ({ state }) => {
    const [mapState, setMapState] = useState(mapStateDefaultValue);

    const updateStep = useCallback(() => {
        setMapState((mapState) => {
            let currentMower = mapState.currentMower;
            let currentStep = mapState.currentStep + 1;
            if (currentMower >= mapState.mowers.length) {
                return mapState;
            } else if (currentStep >= mapState.mowers[currentMower].steps.length) {
                currentMower += 1;
                currentStep = 0;
                return { ...mapState, currentMower, currentStep, currentCommand: "" };
            } else {
                mapState.mowers[currentMower].position = mapState.mowers[currentMower].steps[currentStep];
                return { ...mapState, currentMower, currentStep, currentCommand: mapState.mowers[currentMower].steps[currentStep].command };
            }
        }, []);
    }, []);

    useEffect(() => {
        const newMapState = { ...mapStateDefaultValue, ...state };
        for (const mower of state.mowers) {
            mower.position = mower.steps[0];
        }
        setMapState(newMapState);
    }, [state]);

    useEffect(() => {
        const timeout = setTimeout(updateStep, 1000);
        return () => clearTimeout(timeout);
    }, [mapState, updateStep]);

    const rows = useMemo(() => {
        const rows = [];
        for (let i = mapState.mapSize.y; i >= 0; i--) {
            rows.push(<Row key={i} mapState={mapState} y={i} />);
        }
        return rows;
    }, [mapState]);

    return (
        <div className="mb-2">
            <div className="text-center">
                <div>Current mower: {mapState.currentMower}</div>
                <div>Current step: {mapState.currentStep}</div>
                <div>Current command: {mapState.currentCommand}</div>
            </div>
            <div>
                {rows}
            </div>
        </div>
    );
};

export const Animation = () => {
    const { state } = useResultsContext();
    const showAnimation = useMemo(() => containsSteps(state), [state]);

    if (showAnimation) {
        return (<Map state={state} />);
    } else {
        return null;
    }
}