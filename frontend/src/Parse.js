const getMapSize = (size) => {
    const regex = /([0-9])([0-9])/;
    const matches = size.match(regex);
    if (matches?.length !== 3) {
        throw new Error("Failed to get map's size");
    }
    const coord = { x: +matches[1], y: +matches[2] };
    return coord;
};

const getMowersData = (lines, mapSize) => {
    if (lines.length % 2 !== 0) {
        throw new Error("Wrong number of lines when parsing mowers's data");
    }
    const regex = /([0-9])([0-9]) +([SWNE])/;
    const mowersData = [];
    for (let i = 0; i < lines.length; i += 2) {
        const matches = lines[i].match(regex);
        if (matches?.length !== 4) {
            throw new Error("Failed to get mower's informations");
        }
        const mowerData = {
            start: {
                x: +matches[1],
                y: +matches[2],
                orientation: matches[3]
            },
            commands: lines[i + 1]
        };
        if (mowerData.start.x > mapSize.x || mowerData.start.y > mapSize.y) {
            throw new Error("One of the mower is out of bound");
        }
        mowersData.push(mowerData);
    }
    return mowersData;
};

const regexCheckFileFormat = /[0-9]{2}((\r?\n)?[0-9]{2} +[SWNE]\r?\n[GDA]+)+/;

export const loadFile = (file) => new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.onerror = () => {
        reject(new Error("Error while loading the file"));
    };
    fileReader.onload = () => {
        const content = fileReader.result;
        if (!regexCheckFileFormat.test(content)) {
            reject(new Error("Wrong file format"));
        } else {
            const lines = content.split(/\r?\n/);
            try {
                const mapSize = getMapSize(lines[0]);
                const mowersData = getMowersData(lines.slice(1), mapSize);
                resolve({ mapSize: mapSize, mowers: mowersData });
            } catch (err) {
                reject(err);
            }
        }
    };
    fileReader.readAsText(file, "utf8");
});