import './App.css';
import { ResultsContextProvider } from './ResultsContext';
import { FileForm } from "./Form";
import { Results } from './Results';

export const App = () => {
  return (
    <div>
      <header className="App-header">
        <div className="container my-3">
          <ResultsContextProvider>
            <FileForm />
            <Results />
          </ResultsContextProvider>
        </div>
      </header>
    </div>
  );
};
