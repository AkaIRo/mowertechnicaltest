# Backend

## Installation

Clone the project and run `yarn install`  
  
Start the development version using `yarn start`  
To start in production mode, run `yarn build` and then `yarn prod`

The program can start in two mode:  
- The server mode, which will start a HTTP server with an API endpoint
- The local file mode, which will read files from paths given in the command line

## Server mode

To start in server mode just run the program using `yarn start` or `yarn prod`  
  
The server's hostname and port can be defined in the .env file  
  
The server has two endpoint

`/` :
- Return a 200 status

`/mow` :
- Have optional parameters `?steps` and `?collision`, with any values for both
- Need a JSON body based on the `Data` interface defined in `/src/Interfaces.ts`
- Return a 400 error if the body format is wrong
- On success return a 200 status and JSON body based on the `Result` interface defined in `/src/Interfaces.ts`

## From file

To start in local file mode, add paths to files in the command line when running the program:
- Ex: `yarn start ./test.txt`  
  
You can pass multiples files this way, and all the results will be log in the given order in the terminal