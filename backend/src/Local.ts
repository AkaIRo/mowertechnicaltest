import fs from "fs/promises";
import { Controller } from "Controller";
import { Coord, MowerData } from "Interfaces";
import { CardinalPoint } from "CardinalPoint";

const getMapSize = (size: string): Coord => {
    const regex = /([0-9])([0-9])/;
    const matches = size.match(regex);
    if (matches?.length !== 3) {
        throw new Error("Failed to get map's size");
    }
    const coord: Coord = { x: +matches[1], y: +matches[2] };
    return coord;
};

const getMowersData = (lines: string[], mapSize: Coord): MowerData[] => {
    if (lines.length % 2 !== 0) {
        throw new Error("Wrong number of lines when parsing mowers's data");
    }
    const regex = /([0-9])([0-9]) +([SWNE])/;
    const mowersData: MowerData[] = [];
    for (let i = 0; i < lines.length; i += 2) {
        const matches = lines[i].match(regex);
        if (matches?.length !== 4) {
            throw new Error("Failed to get mower's informations");
        }
        const mowerData = {
            start: {
                x: +matches[1],
                y: +matches[2],
                orientation: matches[3] as CardinalPoint
            },
            commands: lines[i + 1]
        };
        if (mowerData.start.x > mapSize.x || mowerData.start.y > mapSize.y) {
            throw new Error("One of the mower is out of bound");
        }
        mowersData.push(mowerData);
    }
    return mowersData;
};

export const loadFromFile = async (paths: string[]) => {
    for (const path of paths) {
        try {
            const content = await fs.readFile(path, { encoding: "utf-8" });
            const regex = /[0-9]{2}((\r?\n)?[0-9]{2} +[SWNE]\r?\n[GDA]+)+/;
            if (!regex.test(content)) {
                throw new Error("Wrong format");
            }
            const lines = content.split(/\r?\n/);
            const mapSize = getMapSize(lines[0]);
            const mowersData = getMowersData(lines.slice(1), mapSize);
            const controller = new Controller({ mapSize, mowers: mowersData });
            const result = controller.execute();
            if (paths.length > 1) {
                console.log(path);
            }
            for (const mower of result.mowers) {
                console.log(mower.position);
            }
        } catch (err) {
            console.error(`An error occured while reading following file: ${path}`);
            console.error(err);
        }
    }
};