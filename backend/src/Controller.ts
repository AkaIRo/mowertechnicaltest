import { Coord, Data, Result } from "./Interfaces";
import { Mower } from "./Mower";

export class Controller {
    mapSize: Coord;
    mowers: Mower[];

    constructor(data: Data) {
        this.mapSize = data.mapSize;
        this.mowers = [];
        for (const mowerData of data.mowers) {
            this.mowers.push(new Mower(mowerData));
        }
    }

    execute(withSteps: boolean = false, withCollision: boolean = false): Result {
        const result: Result = { mapSize: this.mapSize, mowers: [] };
        for (const mower of this.mowers) {
            const steps = mower.executeCommands(this.mapSize, withCollision ? this.mowers : undefined);
            result.mowers.push({ position: mower.toString(), steps: withSteps ? steps : undefined });
        }
        return result;
    }
}