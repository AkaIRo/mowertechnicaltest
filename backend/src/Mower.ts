import { CardinalPoint, CardinalPointUtils } from "./CardinalPoint";
import { Coord, MowerCoord, MowerStep, MowerData } from "./Interfaces";

export class Mower implements MowerCoord {
    x: number;
    y: number;
    orientation: CardinalPoint;
    commands: string;

    constructor(data: MowerData) {
        this.x = data.start.x;
        this.y = data.start.y;
        this.orientation = data.start.orientation;
        this.commands = data.commands;
    }

    rotateLeft() {
        this.orientation = CardinalPointUtils.rotateLeft(this.orientation);
    }

    rotateRight() {
        this.orientation = CardinalPointUtils.rotateRight(this.orientation);
    }

    /**
     * Move the mower forward
     * Do nothing if the mower go out of bound or if a collision is detected
     * Collision check is only done if "others" arguments is given
     */
    moveForward(size: Coord, others?: Coord[]) {
        let newCoord: Coord = { x: this.x, y: this.y };
        switch (this.orientation) {
            case "N":
                newCoord.y += 1;
                break;
            case "S":
                newCoord.y -= 1;
                break;
            case "E":
                newCoord.x += 1;
                break;
            case "W":
                newCoord.x -= 1;
                break;
        }

        /** Out of bound check */
        if (newCoord.x >= 0 && newCoord.x <= size.x && newCoord.y >= 0 && newCoord.y <= size.y) {
            /** Optional check for collision */
            if (others) {
                for (const coord of others) {
                    if (newCoord.x === coord.x && newCoord.y === coord.y) {
                        return;
                    }
                }
            }
            this.x = newCoord.x;
            this.y = newCoord.y;
        }
    }

    executeCommands(size: Coord, others?: Coord[]): MowerStep[] {
        const steps: MowerStep[] = [];
        steps.push({
            x: this.x,
            y: this.y,
            orientation: this.orientation,
            command: null
        });
        for (const command of this.commands) {
            switch (command) {
                case "D":
                    this.rotateRight();
                    break;
                case "G":
                    this.rotateLeft();
                    break;
                case "A":
                    this.moveForward(size, others);
                    break;
            }
            steps.push({
                x: this.x,
                y: this.y,
                orientation: this.orientation,
                command: command
            });
        }
        return steps;
    }

    toString(): string {
        return `${this.x}${this.y}${this.orientation}`;
    }
}