export type CardinalPoint = "N" | "E" | "S" | "W";

export namespace CardinalPointUtils {
    export const isTypeCardinalPoint = (obj: any) => {
        return (
            typeof obj === "string" &&
            (obj === "N" || obj === "E" || obj === "S" || obj === "W")
        );
    }

    export function rotateLeft(point: CardinalPoint): CardinalPoint {
        switch(point) {
            case "N":
                return "W";
            case "W":
                return "S";
            case "S":
                return "E";
            case "E":
                return "N";
        }
    }

    export function rotateRight(point: CardinalPoint): CardinalPoint {
        switch(point) {
            case "N":
                return "E";
            case "E":
                return "S";
            case "S":
                return "W";
            case "W":
                return "N";
        }
    }
}