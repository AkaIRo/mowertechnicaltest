import Express from "express";
import Cors from "cors";
import { Controller } from "Controller";
import { isTypeData } from "Interfaces";
import { loadFromFile } from "Local";

const startServer = () => {
    const port = (process.env.SERVER_PORT && parseInt(process.env.SERVER_PORT)) || 8080;
    const hostname = process.env.SERVER_HOSTNAME || "localhost";

    const app = Express();

    app.use(Cors({
        origin: "http://localhost:3000"
    }));

    app.get("/", (req, res) => {
        res.sendStatus(200);
    });

    app.post("/mow", Express.json(), (req, res) => {
        if (!isTypeData(req.body)) {
            res.sendStatus(400);
        } else {
            const controller = new Controller(req.body);
            const results = controller.execute("steps" in req.query, "collision" in req.query);
            res.json(results);
        }
    });

    app.listen(port, hostname, () => console.log(`Server listening at http://${hostname}:${port}`));
};


if (process.argv.length > 2) {
    loadFromFile(process.argv.slice(2));
} else {
    startServer();
}