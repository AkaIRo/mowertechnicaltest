import { CardinalPoint, CardinalPointUtils } from "./CardinalPoint";

/** Coord interface and validator */
export interface Coord {
    x: number;
    y: number;
}
export const isTypeCoord = (obj: any) => {
    return (
        typeof obj === "object" && obj !== null &&
        "x" in obj && typeof obj.x === "number" &&
        "y" in obj && typeof obj.y === "number"
    );
};

/** MowerCoord interface and validator */
export interface MowerCoord extends Coord {
    orientation: CardinalPoint;
}
export const isTypeMowerCoord = (obj: any): boolean => {
    return isTypeCoord(obj) && "orientation" in obj && CardinalPointUtils.isTypeCardinalPoint(obj.orientation);
};

/** MowerStep interface and validator */
export interface MowerStep extends MowerCoord {
    command: string | null;
}
export const isTypeMowerStep = (obj: any): boolean => {
    return isTypeMowerCoord(obj) && "command" in obj && (typeof obj.command === "string" || obj.command === null);
};

/** MowerData interface and validator */
export interface MowerData {
    start: MowerCoord;
    commands: string
}
export const isTypeMowerData = (obj: any): boolean => {
    return (
        typeof obj === "object" && obj !== null &&
        "start" in obj && isTypeMowerCoord(obj.start) &&
        "commands" in obj && typeof obj.commands === "string"
    );
};

/** Data interface and validator */
export interface Data {
    mapSize: Coord;
    mowers: MowerData[];
}
export const isTypeData = (obj: any): boolean => {
    return (
        typeof obj === "object" && obj !== null &&
        "mapSize" in obj && isTypeCoord(obj.mapSize) &&
        "mowers" in obj && Array.isArray(obj.mowers) && obj.mowers.every(isTypeMowerData)
    );
}

/** MowerResult interface */
export interface MowerResult {
    position: string;
    steps?: MowerStep[]
}

/** Result interface */
export interface Result {
    mapSize: Coord,
    mowers: MowerResult[]
}